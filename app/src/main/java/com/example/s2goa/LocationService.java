package com.example.s2goa;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.WorkSource;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

// IBinder docs:
// https://developer.android.com/guide/components/bound-services
// https://developer.android.com/reference/android/os/IBinder

// Services tutorial : https://youtu.be/MV2mB1Wev2c

// WORK MANAGER IS USUALLY THE WAY TO GO..
// work manager https://youtu.be/IrKoBFLwTN0
// https://medium.com/google-developer-experts/services-the-life-with-without-and-worker-6933111d62a6

// location stuff
// https://developer.android.com/training/location/request-updates#request-background-location
// https://developer.android.com/training/location/background
// https://youtu.be/_xUcYfbtfsI

// "A Service is an application component that can perform long-running operations in the background, and it does not provide a user interface."
// I'm not sure if its better to use Service or WorkManager....?
// I'm gonna use Service for now...
// nope nevermind, best practices = usually worker
// nope nope nevermind, i dont know

public class LocationService extends Worker {

    /**
     * Location Service
     * */

    public LocationService(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        return null;
    } // last minute saturday

}

