package com.example.s2goa;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    EditText passwordEditText, usernameEditText, emailEditText;
    ImageButton googleSignInButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        passwordEditText=findViewById(R.id.editTextTextPassword);
        usernameEditText=findViewById(R.id.editTextTextPersonUsername);
        emailEditText=findViewById(R.id.editTextTextEmailAddress);
        googleSignInButton=findViewById(R.id.google_signIn_button);

        // TODO: login and account management
        // https://firebase.google.com/docs/auth/android/google-signin?utm_source=studio
        // https://firebase.google.com/docs/reference/android/com/google/firebase/auth/FirebaseAuth.AuthStateListener
        // https://firebase.google.com/docs/auth/android/password-auth#create_a_password-based_account
        // https://firebase.google.com/docs/auth/android/manage-users?utm_source=studio
        super.onCreate(savedInstanceState);
    }
}
