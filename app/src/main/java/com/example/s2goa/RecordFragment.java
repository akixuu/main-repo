package com.example.s2goa;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class RecordFragment extends Fragment {

    ImageButton recordPlayStopButton;

    private static final int ACCESS_FINE_LOCATION_REQUEST_CODE =1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recordPlayStopButton = getActivity().findViewById(R.id.fragment_record_play_button);
        recordPlayStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPermission();
            }
        });
    } // end of onCreate

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        return super.onCreateView(inflater, container, savedInstanceState);
    } // end of onCreateView




    // all permission handling go here

    private void getPermission() {
        // i could make this into a if statement, but this looks better :\
        // should i move the permission check to a different method altogether?
        // returns PERMISSION_DENIED of PERMISSION_GRANTED
        switch (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            case PackageManager.PERMISSION_GRANTED:
                // TODO: we have permissions, then what?
                break;
            case PackageManager.PERMISSION_DENIED:
                // ask user for permission
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, ACCESS_FINE_LOCATION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // perms granted
                    // TODO:
                } else {
                    // WE JUSt GOT DENIED, SAD
                    Toast.makeText(getContext(), "To use this feature, grant location permissions", Toast.LENGTH_LONG).show();
                }
        }
    }

    // its cold :(
}
