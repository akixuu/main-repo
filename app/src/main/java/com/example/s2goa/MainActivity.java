package com.example.s2goa;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        database = openOrCreateDatabase("tableName", SQLiteDatabase.CREATE_IF_NECESSARY, null)
//
//        String queryString = "Create Table TABLE (id INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT)"
//        database.execSQL(queryString);

        DBH dbh = new DBH(MainActivity.this);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {

            // TODO: User is not signed in, so launch login
            // return Intente intent new intent iafj;asdjfl (something like this? )
        }

        // if they are not, setContentView(R.layout.activity_login);
        setContentView(R.layout.activity_main);

        // set bottom navigation
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationListener);

        // set default fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,
                new StatsFragment()
        ).commit();

    } // end of onCreate

    // bottom nav listener TODO: nav graph? navgraph best practicw? afefjlasjf;lakj :((((
    // how did this happen i just clicked the "show context actions" thing what what what
    // i should probably check if its already the current fragment, oh well
    private final BottomNavigationView.OnNavigationItemSelectedListener navigationListener =
            item -> {
                Fragment selectedFragment = null;

                switch (item.getItemId()) {

                    case R.id.nav_record:
                        selectedFragment = new RecordFragment();
                        break;

                    case R.id.nav_quests:
                        selectedFragment = new QuestsFragment();
                        break;

                    case R.id.nav_stats:
                        selectedFragment = new StatsFragment();
                        break;
                }

                assert selectedFragment != null;
                getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment,
                        selectedFragment).commit();

                return true;
            };
}