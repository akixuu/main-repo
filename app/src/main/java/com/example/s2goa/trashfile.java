package com.example.s2goa;

// ImageButton playAndStopImageButton;
/*
private static final int DEFAULT_UPDATE_INTERVAL = 30;
private static final int FAST_UPDATE_INTERVAL = 5;
private static final int PERMISSION_FINE_LOCATIONS = 99;

        LocationCallback locationCallBack;

        // Location request is a config file for all settings related to FusedLocationProvider
        LocationRequest locationRequest;

        // Google's API for location services
        FusedLocationProviderClient fusedLocationProviderClient;

@Override
public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playAndStopImageButton = getActivity().findViewById(R.id.fragment_record_play_button);


        locationRequest = new LocationRequest();

        // set all properties of LocationRequest
        locationRequest.setInterval(1000 * DEFAULT_UPDATE_INTERVAL); // *in ms
        locationRequest.setFastestInterval(1000 * FAST_UPDATE_INTERVAL);
        locationRequest.setPriority(locationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        // triggered even whenever update interval met
        locationCallBack = new LocationCallback() {
@Override
public void onLocationResult(@NonNull LocationResult locationResult) {
        super.onLocationResult(locationResult);
        // locationResult.getLastLocation();
        }
        };

        } // end onCreate method

@Nullable
@Override
public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        playAndStopImageButton.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {
        permissions();
        }

        });

        return super.onCreateView(inflater, container, savedInstanceState);
        } // end of onCreateView

@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
        case PERMISSION_FINE_LOCATIONS:
        // if the request code was PERMISSION_FINE_LOCATIONS
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        } else {
        // if permissions weren't granted
        Toast.makeText(getContext(), "To use this feature you must grant location access", Toast.LENGTH_SHORT).show();
        }

        }
        }

private void stopRun() {
        // stop location tracking
        fusedLocationProviderClient.removeLocationUpdates(locationCallBack);
        // endCardPopup(); and reset layouts
        }

private void permissions() {
        // permissions not granted?
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        // ask for permissions
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATIONS);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallBack, null);
        }
        }
*/
//    private void ui(@NotNull Location location) {
//        tv_lat.setText(String.valueOf(location.getLatitude()));
//        tv_lon.setText(String.valueOf(location.getLongitude()));
//
//        if (location.hasSpeed()) { tv_alt.setText(String.valueOf(location.getSpeed())); }
//        else { tv_alt.setText("Not available"); }
//
//        // get location details, i.e address, phone, etc
//        Geocoder geocoder = new Geocoder(getContext());
//        try {
//            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//            tv_add.setText(addresses.get(0).getAddressLine(0)); // TODO: lots of info
//        }
//        catch (Exception e) {
//            tv_add.setText("Unable to get address");
//        }
//    }
//fusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
//                @Override
//                public void onSuccess(Location location) {
//                    // got perms
//                    startRun();
//                }
//            });
