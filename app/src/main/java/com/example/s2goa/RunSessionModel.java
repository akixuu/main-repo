package com.example.s2goa;

import android.media.Rating;

import java.sql.Time;
import java.sql.Date;

// SQLite Database for Android - Full Course: https://youtu.be/312RhjfetP8

public class RunSessionModel {

    /** Run Session Data Model */

    // empty constructor in case
    public RunSessionModel() { }

    public RunSessionModel(int id, Date date, Time startTime, Time endTime, int distanceTravelled, String note, Rating rating) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.distanceTravelled = distanceTravelled;
        this.note = note;
        this.rating = rating;
    }

    // apparently there's a debate about whether or not to have the id in the class or not. huh.
    private int id;
    private Date date;
    private Time startTime, endTime;
    private int distanceTravelled;
    private String note;
    private Rating rating;

    // can i just say, I LOVE THE GENERATE THING, HOLY IS IT CONVENIENT

    @Override
    public String toString() {
        return "RunSessionModel{" +
                "id=" + id +
                ", date=" + date +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", distanceTravelled=" + distanceTravelled +
                ", note='" + note + '\'' +
                ", rating=" + rating +
                '}';
    }


    // getters and setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public int getDistanceTravelled() {
        return distanceTravelled;
    }

    public void setDistanceTravelled(int distanceTravelled) {
        this.distanceTravelled = distanceTravelled;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }
}
