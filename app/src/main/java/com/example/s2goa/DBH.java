package com.example.s2goa;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

// HAHAH DBH LOLOLol

public class DBH extends SQLiteOpenHelper {
    
    public static final String RUN_SESSION_TABLE = "RUN_SESSION_TABLE";
    public static final String COLUMN_RATING = "COLUMN_RATING";
    public static final String COLUMN_NOTE = "COLUMN_NOTE";
    public static final String COLUMN_DISTANCE_TRAVELLED = "COLUMN_DISTANCE_TRAVELLED";
    public static final String COLUMN_START_TIME = "COLUMN_START_TIME";
    public static final String COLUMN_END_TIME = "COLUMN_END_TIME";
    public static final String COLUMN_DATE = "COLUMN_DATE";
    public static final String COLUMN_ID = "COLUMN_ID";

    public DBH(@Nullable Context context) {
        super(context, "run_session.db", null, 1);
    }

    // will be called the first time a database is accessed, code to create new db
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableStatement = "CREATE TABLE " + RUN_SESSION_TABLE + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, TEXT " + COLUMN_DATE + ", TEXT " + COLUMN_START_TIME + ", TEXT " + COLUMN_END_TIME + ", REAL " + COLUMN_DISTANCE_TRAVELLED + ", TEXT " + COLUMN_NOTE + ", TEXT " + COLUMN_RATING + ")";
        db.execSQL(createTableStatement);
    }


    // called db version number changes
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addOne(RunSessionModel runSessionModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        // kind of like a HashMap (key + values)
        ContentValues contentValues = new ContentValues();

        // id will auto increment, so no need to specify
        contentValues.put(COLUMN_DATE, runSessionModel.getDate().toString());
        contentValues.put(COLUMN_START_TIME, runSessionModel.getStartTime().toString());
        contentValues.put(COLUMN_END_TIME, runSessionModel.getEndTime().toString());
        contentValues.put(COLUMN_DISTANCE_TRAVELLED, runSessionModel.getDistanceTravelled());
        contentValues.put(COLUMN_NOTE, runSessionModel.getNote());
        contentValues.put(COLUMN_RATING, runSessionModel.getRating().toString());

        // db.insert(RUN_SESSION_TABLE, , );
        return true;
    }
    // FIXME: DBH

    // note
    // TODO: next time add friends on steam, to see what they are playing to check if they are working... LOL

}
